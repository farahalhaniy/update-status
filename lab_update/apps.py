from django.apps import AppConfig


class LabUpdateConfig(AppConfig):
    name = 'lab_update'
