from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}
def index(request):    
    response['author'] = "" #TODO Implement yourname
    status = Status.objects.all()
    response['status'] = status
    html = 'lab_update/lab_update.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/lab-update/')
    else:
        return HttpResponseRedirect('/lab-update/')
