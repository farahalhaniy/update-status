from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status
from .models import Status
from .forms import Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class LabUpdateUnitTest(TestCase):

    def test_labupdate_using_index_func(self):
        found = resolve('/lab-update/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_activity = Status.objects.create(description='mengerjakan lab_update ppw')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="status-form-textarea', form.as_p())
        self.assertIn('id="id_description', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_root_url_now_is_using_index_page_from_lab_update(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/lab-update/',301,200)
